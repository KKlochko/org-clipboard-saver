;;; org-clipboard-saver.el --- Save image from clipboard to assets directory.
;;
;; Copyright (c) 2022-2023 Kostiantyn Klochko
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; Author: Kostya Klochko <kostya_klochko@ukr.net>
;; Version: 0.2.0
;; Keywords: org, clipboard, screenshot, save
;; URL: https://gitlab.com/KKlochko/org-clipboard-saver
;; Created: 2022
;; Compatibility: GNU Emacs 24.x
;; Package-requires:
;;
;; This file is NOT part of GNU Emacs.
;;
;;; Code

(defun is-image-in-clipboard ()
  "Return t (true) if an image in the clipboard, else nil."
  (not
   (string=
    (shell-command-to-string "xclip -o -selection clipboard -t TARGETS | grep \'image/\'") "")))

(defun org-clipboard-saver-assets-screenshot ()
  "Take a screenshot from the clipboard and save a file in to the
  ../assets/ directory with a name that contains the current buffer name,
  timestamp and unique prefix. Also insert a link to this file."
  (interactive)
  (let* ((file
	  (concat
	   (make-temp-name
	    (concat (buffer-file-name)
		    "_"
		    (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))

	 (screenshot-file-name
	  (file-name-nondirectory file))

	 (screenshot-file
	  (concat
	   (file-name-directory
	    (directory-file-name
	     (file-name-directory file)))
	   "assets/" screenshot-file-name)))

    (if (is-image-in-clipboard)
	(progn
	  (shell-command (concat "xclip -selection clipboard -target image/png -out > ../assets/" screenshot-file-name))
	  (insert (concat "[[" screenshot-file "][" screenshot-file-name "]]"))
	  (org-display-inline-images))
      (message "No image in the clipboard!!!"))))

(defun org-clipboard-saver-assets-screenshot-relative ()
  "Take a screenshot from the clipboard and save a file in to the
  ../assets/ directory with a name that contains the current buffer name,
  timestamp and unique prefix. Also insert a link to this file is relative
  to current file."
  (interactive)
  (let* ((file
	  (concat
	   (make-temp-name
	    (concat (buffer-file-name)
		    "_"
		    (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))

	 (screenshot-file-name
	  (file-name-nondirectory file))

	 (screenshot-file
	  (concat
	   "../assets/" screenshot-file-name)))

    (if (is-image-in-clipboard)
	(progn
	  (shell-command (concat "xclip -selection clipboard -target image/png -out > " screenshot-file))
	  (insert (concat "[[" screenshot-file "][" screenshot-file-name "]]"))
	  (org-display-inline-images))
      (message "No image in the clipboard!!!"))))

(provide 'org-clipboard-saver)
