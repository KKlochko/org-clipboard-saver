* org-clipboard-saver
org-clipboard-saver is an extension that add functions for saving
screenshots or images from clipboard to the assets folder.

* assets folder path
My structure in a folder:
- assets
- org-directory

* dependencies
The extension depends on the app:
- xclip

* Author
Kostiantyn Klochko (c) 2022-2023

* Donation
Monero: 89Amxfx4ur7MzLPhUsTUhYgG3oSbvrb36DQwoopMzJoZQZojYFnnzLASG9jufU8XYyPNv6kpVKQseeGwNU5LaFne9gBEqUt
[[./img/monero.png]]

* License
Under GNU GPL v3 license
